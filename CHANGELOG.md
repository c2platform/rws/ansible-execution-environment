# CHANGELOG

## 0.1.27 (2025-2-20 )

* Upgrade `ansible-core` → `2.16.0`

## 0.1.26 ( 2025-1-31 )

* Upgrade `ansible-lint` → `25.1.1`

## 0.1.25 ( 2024-12-19 )

* Upgrade `ansible-lint` → `24.12.2`, `yamllint` → `1.35.1`.
* Upgrade collections `gis:1.0.21` → `wincore:1.0.15` → `core::1.0.24`.

## 0.1.24 ( 2024-08-15 )

* `c2platform.gis` → `1.0.18`.

## 0.1.23 ( 2024-07-12 )

* `c2platform.gis` → `1.0.17`.

## 0.1.22 ( 2024-06-26 )

* `c2platform.gis` → `1.0.15`.

## 0.1.21 ( 2024-06-26 )

* `ansible-lint==24.6.1`
* `git`

## 0.1.20 ( 2024-06-26 )

* `c2platform.core` → `1.0.22`
* `c2platform.dev` → `1.0.4`
* `c2platform.gis` → `1.0.14`.
* `c2platform.mgmt` → `1.0.4`
* `c2platform.mw` → `1.0.3`
* `c2platform.wincore` → `1.0.13`

## 0.1.19 ( 2024-05-30 )

* `c2platform.gis` → `1.0.12`.

## 0.1.18 ( 2024-05-02 )

* `c2platform.core` → `1.0.18`
* `c2platform.wincore` → `1.0.12`.

## 0.1.17 ( 2024-04-18 )

* `c2platform.gis` → `1.0.11`.
* `c2platform.wincore` → `1.0.10`.
* `c2platform.core` → `1.0.16`.

## 0.1.16 ( 2024-04-18 )

* `c2platform.gis` → `1.0.10`.

## 0.1.15 ( 2024-04-16 )

* `c2platform.gis` → `1.0.9`.

## 0.1.14 ( 2024-04-08 )

* `c2platform.core` → `1.0.13`.

## 0.1.13 ( 2024-04-02 )

* `c2platform.gis` → `1.0.8`.

## 0.1.12 ( 2024-03-08 )

* `c2platform.core` → `1.0.12`
* `c2platform.mgmt` → `1.0.3`
* `c2platform.mw` → `1.0.2`
* `c2platform.wincore` → `1.0.7`.

## 0.1.11 ( 2024-02-19 )

* `c2platform.gis` → `1.0.7`.

## 0.1.10 ( 2024-02-16 )

* `c2platform.core` → `1.0.11`.

## 0.1.9 ( 2024-02-14 )

* `c2platform.gis` → `1.0.6`.

## 0.1.8 ( 2024-02-14 )

* Updated `README.md`.

## 0.1.7 ( 2024-02-02 )

* Pipeline using GitLab Runner image with `ansible-builder` so that an EE can be
  created with manual steps.
* Updated `requirements.yml` with all collections.

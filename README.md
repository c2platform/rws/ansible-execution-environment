# Ansible Execution Environment for Rijkswaterstaat (RWS)

[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-execution-environment/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/releases)

This GitLab project provides an Ansible Execution Environment for
Rijkswaterstaat (RWS) that is suitable for Ansible Automation Platform (AAP) and
AWX. The purpose of this execution environment is to have more control over
Python and Ansible versions.

- [Features](#features)
- [Getting Started](#getting-started)
- [Creating a New Image](#creating-a-new-image)
- [Creating a New Release](#creating-a-new-release)
- [Repository Files](#repository-files)
- [More Information](#more-information)


## Features

* Based on `ghcr.io/ansible-community/community-ee-minimal:2.15.4-2` for a more recent Python version.

> Note: This image is also currently used  for c2 GitLab pipelines. As an
> example, the Ansible collection
> [`c2platform.wincore`](https://gitlab.com/c2platform/rws/ansible-collection-wincore)
> utilizes this image. Refer to the `.gitlab-ci.yml` for detailed integration
> instructions.

## Getting Started

1. Clone the GitLab repository to your local machine.
1. Modify the `execution-environment.yml` file to make the necessary changes to
   the environment. This file is used by ansible-builder.
1. Remove the `context` folder.
1. Run `ansible-builder create` to update the `context` folder with a
   `Dockerfile` and a `_build` directory containing all the build artifacts.
1. Perform local testing by running `ansible-builder build` to create and test
   the image locally.
1. Use Ansible Navigator with a test play to verify that the image is working.


## Creating a New Image

To create a new image, you can push your changes to the GitLab repository. This
will trigger a pipeline that automatically creates a new image based on the
latest changes. Update `CHANGELOG.md`.

## Creating a New Release

If you want to create a new release of the execution environment, update the
`CHANGELOG.md` and commit and push your changes.

## Repository Files

1. `.gitlab-ci.yml`: This file contains the CI/CD pipeline configuration. It
   defines the stages and jobs required for building, releasing, and deploying
   the execution environment.
1. `execution-environment.yml`: This file specifies the dependencies and
   additional build steps for creating the execution environment. Modify this
   file to make changes to the environment.
1. `CHANGELOG.md`: This file contains the version information of the execution
   environment. Update this file when creating a new release.


## More Information

For more details information on how this image can be created, tested and used
see
[Create custom Ansible Execution Environment | C2 Platform](https://c2platform.org/en/docs/howto/rws/exe-env)
